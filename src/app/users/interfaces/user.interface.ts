export interface UsersResponse {
  ok:   boolean;
  data: User[];
}

export interface User {
  nombre:    string;
  apellido:  string;
  fullNombre: string;
  email:     string;
  direccion: string;
}


