import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserViewComponent } from './pages/user-view/user-view.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'userView', component: UserViewComponent },
      { path: '**', redirectTo: 'userView' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }

