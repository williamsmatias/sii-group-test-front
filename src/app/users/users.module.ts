import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { UsersRoutingModule } from './users-routing.module';
import { UserViewComponent } from './pages/user-view/user-view.component';
import { UserSearchComponent } from './components/user-search/user-search.component';
import { UsersTableComponent } from './components/users-table/users-table.component';



@NgModule({
  declarations: [UserViewComponent, UserSearchComponent, UsersTableComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule
  ]
})
export class UsersModule { }
