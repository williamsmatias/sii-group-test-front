import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User, UsersResponse } from '../interfaces/user.interface';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlService: string = environment.baseUrl;

  constructor( private http: HttpClient ) { }

  obtenerUsuarios(): Observable<User[]> {
    const url = `${ this.urlService }/users`;

    return this.http.get<UsersResponse>(url)
              .pipe(
                map( (resp) => {
                  let users: User[] = [];
                  if (resp.ok) {
                    users = resp.data.map( user => ( { ...user, fullNombre : user.nombre + " " + user.apellido} ) );
                  }
                  return users;
                })
              );
  }

}
