import { Component } from '@angular/core';
import { User } from '../../interfaces/user.interface';
import { UserService } from '../../services/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent {

  termino: string = '';
  error: boolean = false;
  usuarios: User[] = [];
  usuariosSugeridos: User[] = [];
  mostrarSugeridos: boolean = false;

  constructor( private userService: UserService ) { }


  buscar( user: User = undefined ): void {
    if( user ){
      this.usuarios = [];
      this.usuarios.push(user);
      this.mostrarSugeridos = false;
      this.usuariosSugeridos = [];
      return;
    }

    if ( this.usuariosSugeridos.length === 0 ) {
      this.usuarios = [];
      this.error = true;
      return;
    }

    this.usuarios = this.usuariosSugeridos;
    this.mostrarSugeridos = false;
  }

  sugerencias(termino: string){
    if ( termino.trim().length === 0 ) {
      this.termino = '';
      this.usuariosSugeridos = []
      return;
    }
    this.error = false;
    this.mostrarSugeridos = true;
    this.termino = termino;
    this.userService.obtenerUsuarios()
    .pipe(
      map( resp => resp.filter( user => user.fullNombre.indexOf(termino) !== -1 ) )
      )
    .subscribe( (users) => {
        this.usuariosSugeridos = users.splice(0,5);
      },
      (err) => this.usuariosSugeridos = []
      );
  }

}
